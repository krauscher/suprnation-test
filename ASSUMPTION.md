# Assumptions

The algorithm by itself does not make much assumptions per se

However the code does not go to great length to handle user input errors. This stems from the debatable assumption than in an exercice of this kind there is not much more to do than crash when incorrect user inputs are given.

A special focus has been given to property tests, even though creating correct data is quite painful, as it is really easy to miss a corner case.
