import sbt._

object Dependencies {
  lazy val cats = "org.typelevel" %% "cats-core" % "2.0.0"
  lazy val catsEffect = "org.typelevel" %% "cats-effect" % "2.1.2"
  lazy val fs2Core =
    "co.fs2" %% "fs2-core" % "2.2.1" // For cats 2 and cats-effect 2
  lazy val fs2IO = "co.fs2" %% "fs2-io" % "2.2.1"
  lazy val newtype = "io.estatico" %% "newtype" % "0.4.3"

  lazy val scalaCheck = "org.scalacheck" %% "scalacheck" % "1.14.3"
  lazy val scalaTestScalaCheck =
    "org.scalatestplus" %% "scalacheck-1-14" % "3.1.1.1"
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.8"
}
