package fr.tomahna.suprnation.model

import cats.Show

case class SolverResult(value: List[Triangle.Value]) extends AnyVal
object SolverResult {
  implicit val show: Show[SolverResult] =
    result =>
      s"""Minimal path is: ${result.value.mkString(" + ")} = ${result.value
        .map(_.value)
        .sum}"""
}
