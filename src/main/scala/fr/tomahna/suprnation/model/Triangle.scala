package fr.tomahna.suprnation.model

import io.estatico.newtype.macros.newtype
import cats.kernel.Semigroup

object Triangle {
  @newtype case class Value(value: Int)
  object Value {
    implicit val semigroup: Semigroup[Value] =
      (a, b) => Value(a.value + b.value)
  }
}
