package fr.tomahna.suprnation

import cats.instances.int.catsKernelStdOrderForInt
import cats.syntax.all._
import fr.tomahna.suprnation.model.SolverResult
import fr.tomahna.suprnation.model.Triangle

object MinimumPathSolver {

  def solve(triangle: List[List[Triangle.Value]]): SolverResult = {
    SolverResult(
      triangle
        .drop(1)
        .foldLeft[List[List[Triangle.Value]]](List(triangle.head))(minPaths)
        .minBy(_.map(_.value).sum)
        .reverse
    )
  }

  def minPaths(
      minPaths: List[List[Triangle.Value]],
      row: List[Triangle.Value]
  ): List[List[Triangle.Value]] =
    row.zipWithIndex.map {
      case (value, index) =>
        if (index === 0) value :: minPaths(index)
        else if (index === row.length - 1) value :: minPaths(index - 1)
        else {
          val firstParent = minPaths(index - 1)
          val secondParent = minPaths(index)
          if (firstParent.map(_.value).sum < secondParent.map(_.value).sum)
            value :: firstParent
          else value :: secondParent
        }
    }
}
