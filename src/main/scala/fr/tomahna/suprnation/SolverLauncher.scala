package fr.tomahna.suprnation

import fr.tomahna.suprnation.model.Triangle
import cats.instances.byte.catsKernelStdOrderForByte
import cats.instances.string.catsKernelStdOrderForString
import cats.syntax.all._
import cats.effect._
import fs2.io._

object SolverLauncher extends IOApp {
  private val blocker =
    Blocker.liftExecutionContext(scala.concurrent.ExecutionContext.global)

  override def run(args: List[String]): IO[ExitCode] =
    for {
      triangle <- inputTriangle[IO].compile.toList
      result = MinimumPathSolver.solve(triangle)
      _ <- printIO(result.show)
    } yield ExitCode.Success

  private def printIO(message: String): IO[Unit] = IO(println(message))

  private def inputTriangle[F[_]: Sync: ContextShift]
      : fs2.Stream[F, List[Triangle.Value]] =
    stdin[F](1024, blocker)
      .split(_ === '\n'.toByte)
      .filter(_.nonEmpty)
      .through(fs2.text.utf8DecodeC)
      .takeWhile(_ =!= "EOF")
      .evalMap(parseLine[F])

  private def parseLine[F[_]: Sync](line: String): F[List[Triangle.Value]] =
    Sync[F].catchNonFatal {
      line
        .split(" ")
        .toList
        .filter(_.nonEmpty)
        .map(i => Triangle.Value(i.toInt))
    }

}
