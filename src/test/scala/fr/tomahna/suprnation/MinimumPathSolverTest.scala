package fr.tomahna.suprnation

import fr.tomahna.suprnation.model.SolverResult
import fr.tomahna.suprnation.model.Triangle
import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class MinimumPathSolverTest
    extends AnyFlatSpec
    with Matchers
    with TypeCheckedTripleEquals {

  """Solving
        7
      6   3
    3   8   5
  11  2   10  9
  """ should "yield 7 + 6 + 3 + 2" in {
    val triangle = List(
      List(7),
      List(6, 3),
      List(3, 8, 5),
      List(11, 2, 10, 9)
    ).map(_.map(Triangle.Value.apply))

    val solution = MinimumPathSolver.solve(triangle)
    solution should ===(
      SolverResult(
        List(
          Triangle.Value(7),
          Triangle.Value(6),
          Triangle.Value(3),
          Triangle.Value(2)
        )
      )
    )
  }
  """Solving
  1
  47 1
  1 17 47
  40 1 28 17
  """ should "yield 4" in {
    val triangle = List(
      List(1),
      List(1, 47),
      List(1, 17, 47),
      List(40, 1, 28, 17)
    ).map(_.map(Triangle.Value.apply))

    val solution = MinimumPathSolver.solve(triangle)
    solution should ===(
      SolverResult(
        List(
          Triangle.Value(1),
          Triangle.Value(1),
          Triangle.Value(1),
          Triangle.Value(1)
        )
      )
    )
  }

  """Solving
  1
  51 1
  2 70 1
  95 75 1 84
  72 84 54 1 21
  """ should "yield 5" in {
    val triangle = List(
      List(1),
      List(51, 1),
      List(2, 70, 1),
      List(95, 75, 1, 84),
      List(72, 84, 54, 1, 21)
    ).map(_.map(Triangle.Value.apply))

    val solution = MinimumPathSolver.solve(triangle)
    solution.value.map(_.value).sum should ===(5)
  }

  "Solving 7" should "yield 7" in {
    val solution = MinimumPathSolver.solve(List(List(Triangle.Value(7))))
    solution should ===(SolverResult(List(Triangle.Value(7))))
  }

}
