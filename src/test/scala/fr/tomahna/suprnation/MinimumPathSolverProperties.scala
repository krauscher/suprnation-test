package fr.tomahna.suprnation

import fr.tomahna.suprnation.model.Triangle
import org.scalacheck.Arbitrary
import org.scalacheck.Arbitrary._
import org.scalacheck.Gen
import org.scalatest.propspec.AnyPropSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import org.scalatest.matchers.should.Matchers

class MinimumPathSolverProperties
    extends AnyPropSpec
    with Matchers
    with ScalaCheckPropertyChecks {

  property("Solver should work for big examples") {
    implicit val bigTriangle: Arbitrary[List[List[Triangle.Value]]] =
      Arbitrary {
        for {
          values <- Gen.infiniteStream(
            Gen.choose[Int](1, 100).map(Triangle.Value.apply)
          )
        } yield (1 to 500).toList.map(n => values.take(n).toList)
      }

    forAll { (triangle: List[List[Triangle.Value]]) =>
      MinimumPathSolver.solve(triangle).value.size should ===(500)
    }
  }

  /*
   A rigged triangle is a triangle containing one and only one path constituted only of 1
   The rest of the triangle is constituted of number > to 1
   Thus we now the minimum path is equal to the size of the triangle
   */
  property("Solver should find correct path for rigged triangles") {
    def riggedLineGen(
        prevRiggedPos: Int,
        size: Int
    ): Gen[(Int, List[Triangle.Value])] =
      for {
        riggedPos <- Gen.choose[Int](prevRiggedPos, prevRiggedPos + 1)
        values <- Gen.infiniteStream(Gen.choose[Int](2, 100))
      } yield {
        val line =
          values.take(size).toList.updated(riggedPos, 1).map(Triangle.Value(_))
        (riggedPos, line)
      }

    implicit val riggedTriangle: Arbitrary[List[List[Triangle.Value]]] = {
      def riggedTriangleAux(
          prevRiggedPos: Int,
          tail: List[List[Triangle.Value]],
          size: Int,
          maxSize: Int
      ): Gen[List[List[Triangle.Value]]] = {
        if (size < maxSize)
          for {
            (riggedPos, line) <- riggedLineGen(prevRiggedPos, size)
            result <- riggedTriangleAux(
              riggedPos,
              line :: tail,
              size + 1,
              maxSize
            )
          } yield result
        else Gen.const(tail)
      }

      Arbitrary {
        for {
          size <- Gen.choose[Int](2, 20)
          lines <- riggedTriangleAux(0, List(List(Triangle.Value(1))), 2, size)
        } yield lines.reverse
      }
    }

    forAll { (triangle: List[List[Triangle.Value]]) =>
      val solution = MinimumPathSolver.solve(triangle).value
      val pathSize = solution.map(_.value).sum
      pathSize should ===(solution.size)
    }

  }

}
