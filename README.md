# suprnation-test

This code can be run directly via SBT

``` shell
cat datasets/dataset1 | sbt run
```

or "packaged" with sbt native packager

``` shell
sbt stage
cat datasets/dataset1 | target/universal/stage/bin/suprnation-test
```
