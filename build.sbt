import Dependencies._

scalaVersion := "2.13.1"
version := "0.1.0-SNAPSHOT"
organization := "fr.tomahna"

name := "suprnation-test"

addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")

enablePlugins(JavaAppPackaging)

scalacOptions += "-Ymacro-annotations"

libraryDependencies ++= Seq(
  cats,
  catsEffect,
  fs2Core,
  fs2IO,
  newtype,
  scalaCheck % Test,
  scalaTest % Test,
  scalaTestScalaCheck % Test
)

mainClass in (Compile, run) := Some("fr.tomahna.suprnation.SolverLauncher")

fork in Test := true
